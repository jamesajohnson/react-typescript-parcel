# React TypeScript Parcel

A React, TypeScript and Parcel Boiler Plate.

### Start Dev Server
```
npm start
```

### Useful Links
- [React][1]
- [TypeScript][2]
- [Parcel 📦][3]

[1]: https://reactjs.org/ "React"
[2]: https://www.typescriptlang.org/ "TypeScript"
[3]: https://parceljs.org/ "Parcel"