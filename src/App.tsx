import * as React from 'react';
import * as ReactDOM from 'react-dom';

import Hello from './components/Hello/Hello';

class App extends React.Component {

    render(): void {
        return (
            <div>
                <h1>Your New App!</h1>
                <Hello />
            </div>
        );
    }
}

export default App;