import * as React from 'react';

class Hello extends React.Component {
    render(): void {
        return (
           <p><b>What's this? </b>A React, TypeScript &amp; Parcel boiler plate.</p>
        );
    }
}

export default Hello;